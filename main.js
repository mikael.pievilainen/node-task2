const express = require('express');
const app = express();
const PORT = 3000;


// Baseurl: http://localhost:3000
// Endpoint: http://localhost:3000/
app.get('/', (_req, res) => {
    // req = request, res = response
  res.send('Hello World');
});

/**
 * This is arrow function to add two numbers together.
 * @param {Number} a is the first number
 * @param {Number} b is the second number
 * @return {Number} This is the sum of numbers
 */

const add = (a, b) => {
    const sum = a + b;
    return sum;
};

app.use(express.json()); // this is middleware
// endpoint: http://localhost:3000/add
app.post('/add', (req, res) => {
    const a = req.body.a;
    const b = req.body.b;
    const sum = add(a, b);
    res.send(sum.toString());
});

// This starts the server
app.listen(PORT, () => console.log(
    `server listening at http://localhost:${PORT}`
));

console.log("express application starting..");